export default interface Steps {
    id: number,
    text: string,
    content: string
}