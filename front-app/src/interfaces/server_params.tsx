export default interface Server_param {
    name: String,
    Min_client: String,
    Fit_client: String,
    Rounds: String,
    Epochs: String
}