
const Slider = (data: { text: string, onChange: () => void, val: string, min: number, max: number, step: number }): JSX.Element => {

    return (
        <div className="relative pt-1 w-5/6 ml-auto mr-auto" >
            <label htmlFor="customRange3" className="form-label">{data.text} : <span>{data.val}</span></label>
            <input
                onChange={data.onChange}
                type="range"
                className="w-full h-2 bg-slate-200 appearance-none accent-orange-600"
                min={data.min}
                max={data.max}
                step={data.step}
                id="customRange3"
            />
        </div>


    )
}

export default Slider;