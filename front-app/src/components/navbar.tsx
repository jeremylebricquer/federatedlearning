import React from "react";
import { NavLink } from "react-router-dom";

export const NavBar = (): JSX.Element => {
    const [navbarOpen, setNavbarOpen] = React.useState(false);
    return (<>
        <nav className="navbar relative flex flex-wrap items-center justify-between px-2 py-3 bg-gray-900 mb-3">
            <div className="container 2xl:max-w-full px-4 mx-auto flex flex-wrap items-center justify-between">
                <div className="w-full relative flex justify-between lg:w-auto lg:static lg:block lg:justify-start">
                    <a
                        className="text-lg font-extrabold leading-relaxed inline-block mr-4 py-2 whitespace-nowrap uppercase text-white"
                        href="/"
                    >
                        FL
                    </a>
                    <button
                        className="text-white cursor-pointer text-xl leading-none px-3 py-1 border border-solid border-transparent rounded bg-transparent block lg:hidden outline-none focus:outline-none"
                        type="button"
                        onClick={() => setNavbarOpen(!navbarOpen)}
                    >
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true" className="h-6 w-6">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h16M4 18h16">
                            </path>
                        </svg>
                    </button>
                </div>

                <div
                    className={
                        "lg:flex flex-grow " +
                        (navbarOpen ? " flex" : " hidden")
                    }
                    id="example-navbar-danger"
                >
                    <ul className="flex flex-col lg:flex-row list-none lg:ml-auto lg:items-center">
                        <li className="nav-item">
                            <NavLink
                                className="py-2 flex items-center text-xs uppercase font-bold leading-snug text-white hover:opacity-75"
                                to="/"
                            >
                                <span className="ml-2">Home</span>
                            </NavLink>
                        </li>
                        <li className="nav-item lg:px-6 2xl:px-10">
                            <NavLink
                                className="py-2 flex items-center text-xs uppercase font-bold leading-snug text-white hover:opacity-75"
                                to="/"
                            >
                                <span className="ml-2">Login</span>
                            </NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink
                                className="py-2 flex items-center text-xs uppercase font-bold leading-snug text-white hover:opacity-75"
                                to="/"
                            >
                                {navbarOpen
                                    ? <button className="text-orange-600 border-2 border-orange-600 hover:bg-orange-600 hover:text-white active:bg-slate-800 font-semibold uppercase text-xs px-3 py-2 rounded outline-none focus:outline-none mr-1 ease-linear transition-all duration-150 disabled:text-transparent">My account</button>
                                    : <button className="text-orange-600 border-2 border-orange-600 hover:bg-orange-600 hover:text-white active:bg-slate-800 font-bold uppercase text-sm px-6 py-3 rounded outline-none focus:outline-none mr-1 ease-linear transition-all duration-150 disabled:text-transparent">My account</button>}

                            </NavLink>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </>
    )
}