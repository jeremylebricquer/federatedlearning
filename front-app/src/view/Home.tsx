import { Link } from "react-router-dom";
import FlImage from "../assets/35.jpg"

const Home = (): JSX.Element => {
    return (
        <>
            <div className="container mx-auto 2xl:max-w-full">
                <div className="grid gap-4 md:grid-cols-2 py-8 px-4 xl:py-10 xl:px-6" >
                    <div className="flex flex-col justify-evenly">
                        <h2 className="xl:ml-16 2xl:ml-20 text-left mb-4 text-4xl tracking-tight font-extrabold mt-3 max-w-xl mx-auto text-slate-700 md:mt-5 2xl:max-w-screen-2xl">
                            Object classification with <span className="text-orange-600">Federate learning</span>
                        </h2>
                        <div className="max-w-screen-lg">
                            <h3 className="text-left mt-3 max-w-md mx-auto text-lg text-slate-700 sm:text-xl md:mt-5 md:max-w-3xl">
                                Federated learning aims at training a machine learning algorithm,
                                for instance deep neural networks, on multiple local datasets contained in local nodes without explicitly exchanging data samples.
                                The general principle consists in training local models on local data samples and exchanging parameters
                                (e.g. the weights and biases of a deep neural network) between these local nodes at some frequency to generate a global model shared by all nodes.
                            </h3>
                        </div>
                        <div className="grid items-center pt-8 lg:pt-4">
                            <Link to="/FL">
                                <button type="button" className="text-orange-600 border-2 border-orange-600 hover:bg-orange-600 hover:text-white active:bg-slate-800 font-bold uppercase text-sm px-6 py-3 rounded outline-none focus:outline-none mr-1 ease-linear transition-all duration-150 disabled:text-transparent">
                                    Go to federate learning
                                </button>
                            </Link>
                        </div>
                    </div>
                    <div className="flex flex-row justify-center items-center">
                        <img className="border-2 border-black object-cover h-5/6 w-5/6 md:h-5/6  lg:w-4/6  xl:w-3/6 rounded-xl" src={FlImage}></img>
                    </div>
                </div>
            </div>
        </>
    )
}


export default Home;