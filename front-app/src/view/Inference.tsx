import { useState } from "react";
import { inference } from "../api/inference";
import { urlInference } from "../api/url";


const Inference = (d: { disabled: () => void }): JSX.Element => {

    /*   let headers: {
          'Accept': 'application/json',
          'Content-Type': 'multipart/form-data'
      }
  
      const uploadFile = () => {
          var fileInput: any = document.querySelector('#fileInput');
          if (fileInput.files[0]) {
              var formData = new FormData();
              for (const file of fileInput.files)
                  formData.append('files', file);
              d.disabled()
              inference(urlInference, formData, headers)
  
          }
      } */
    let headers: {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data'
    }
    const [selectedFile, setSelectFile] = useState<any>(null);

    const fileChangerHandler = (e: React.ChangeEvent) => {
        const target = e.target as HTMLInputElement;
        const file: File = (target.files as FileList)[0];
        setSelectFile(file);
    }

    const uploadFile = (e: React.MouseEvent<HTMLElement>) => {
        e.preventDefault()
        const formData = new FormData();
        formData.append(
            "file",
            selectedFile,
            selectedFile.name
        )
        d.disabled()
        inference(urlInference, formData, headers)
    }

    return (
        <div className="container mx-auto w-full p-4 md:p-8" >
            < div className="grid grid-cols-1 gap-6 md:grid-cols-2" >
                <div className="basis-1/2 justify-items-center px-4 ">
                    <fieldset className="border-4 border-solid  border-cyan-800 p-3">
                        <legend className="text-2xl text-left px-2.5 font-semibold"> Inférence </legend>
                        <div className="flex flex-col items-center gap-y-4">
                            <label
                                className="flex justify-center w-full h-32 md:h-48 lg:h-60  px-4 transition bg-white border-2 border-gray-300 border-dashed rounded-md appearance-none cursor-pointer hover:border-gray-400 focus:outline-none">
                                <div className="flex items-center w-full relative">
                                    <input
                                        accept=".jpg , .png , .jpeg"
                                        name="file"
                                        onChange={fileChangerHandler}
                                        id="fileInput"
                                        type="file"
                                        className="cursor-pointer absolute block opacity-0 w-full h-full " >
                                    </input>
                                    <div className="flex items-center w-full justify-center -z-1">
                                        <svg xmlns="http://www.w3.org/2000/svg" className="w-6 h-6 text-gray-600" fill="none" viewBox="0 0 24 24"
                                            stroke="currentColor" strokeWidth="2">
                                            <path strokeLinecap="round" strokeLinejoin="round"
                                                d="M7 16a4 4 0 01-.88-7.903A5 5 0 1115.9 6L16 6a5 5 0 011 9.9M15 13l-3-3m0 0l-3 3m3-3v12" />
                                        </svg>
                                        <div className="font-medium text-gray-600">
                                            Drop image files
                                        </div>
                                    </div>

                                </div>

                            </label >
                            <button value="Upload" onClick={uploadFile} className="btn inline-flex items-center px-6 m-5">
                                <svg className="animate-bounce fill-current w-4 h-4 mr-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                    <path d="M13 8V2H7v6H2l8 8 8-8h-5zM0 18h20v2H0v-2z" />
                                </svg>
                                <span>Download</span>
                            </button>
                        </div>
                    </fieldset>
                </div>
                <div className="basis-1/2 justify-items-center px-4">
                    <div className="border-2 border-orange-600 h-full text-3xl text-orange-600 flex flex-col justify-center">
                        Results
                    </div>
                </div>
            </div >
        </div>
    );
};



export default Inference;

