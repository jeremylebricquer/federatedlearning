import { useState, useEffect } from "react";
import { getFactories } from "../api/factories";
import { urlFactories } from "../api/url";
import Factories from "../interfaces/factories";
import Stepper from "./stepper";


const FederateLearning = (): JSX.Element => {

    const [factory, setFactory] = useState<Factories[] | any>([]);

    useEffect(() => {
        const fetch = async () => {
            try {
                const rq = await getFactories(urlFactories);
                setFactory(rq.data);
            } catch (err) {
                console.log(err);
            }
        };
        fetch();
    }, []);

    return (
        <div className="App">
            <div className='content-center flex flex-col'>
                <h1 className="my-8  md:my-12 text-4xl md:text-6xl lg:text-6xl xl:text-8xl font-extrabold text-blue-900 dark:text-white xl:pb-10 lg:pb-8 md:pb-8 sm:pb-6">
                    <span className="text-transparent bg-clip-text bg-gradient-to-r from-orange-700 to-black via-orange-500  animate-gradient-x">Federate learning</span>
                </h1>
                <div className="container mx-auto space-y-5">
                    <Stepper factory={factory} />
                </div>
            </div>
        </div >
    )

}


export default FederateLearning;