import { useState } from "react";
import { server_params } from "../api/server_params";
import { urlServerParams } from "../api/url";
import Alert from "../components/alert";
import Dropdown from "../components/dropdown";
import Slider from '../components/slider'
import Factories from "../interfaces/factories";
import Server_param from "../interfaces/server_params";


const Form = (props: { factory: Factories[], disabled: () => void }): JSX.Element => {

    const [value, setValue] = useState("");
    const [valueS1, setValueS1] = useState("10");
    const [valueS2, setValueS2] = useState("0");
    const [valueS3, setValueS3] = useState("");
    const [valueS4, setValueS4] = useState("");
    const [isError, setIsError] = useState(true);



    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setValue(e.target.value)
    };

    const onChangeS1 = (e: React.ChangeEvent<HTMLInputElement>) => {
        setValueS1(e.target.value)
    }
    const onChangeS2 = (e: React.ChangeEvent<HTMLInputElement>) => {
        setValueS2(e.target.value)
    }
    const onChangeS3 = (e: React.ChangeEvent<HTMLInputElement>) => {
        setValueS3(e.target.value)
    }
    const onChangeS4 = (e: React.ChangeEvent<HTMLInputElement>) => {
        setValueS4(e.target.value)
    }

    const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        if (valueS1 < valueS2) {
            setIsError(false)
            console.log('Error', valueS1, valueS2)
        }
        else {
            let s_params: Server_param = {
                'name': value,
                'Min_client': valueS1,
                'Fit_client': valueS2,
                'Rounds': valueS3,
                'Epochs': valueS4,
            }
            props.disabled()
            server_params(urlServerParams, s_params)
        }

    }

    return (
        <div className="flex flex-row ">

            <form className="w-full p-4 md:p-8" onSubmit={onSubmit}>

                <fieldset className="border-4 border-solid  border-cyan-800 p-4 leading-4 md:leading-6 xl:leading-8 2xl:leading-loose">
                    <legend className="text-2xl text-left px-2.5 font-semibold">FL options</legend>

                    {isError ?
                        < div className="grid gap-4 md:grid-cols-2" >

                            <div className="justify-items-center  ">
                                <div className="flex flex-col items-center gap-y-4">
                                    <span className="text-lg font-medium">Select your factory : </span>
                                    < Dropdown fact={props.factory} handleChange={handleChange} />
                                </div>
                            </div>

                            <div className="justify-items-center">
                                < Slider text="Min_client" onChange={onChangeS1} val={valueS1} min={2} max={10} step={1} />
                                < Slider text="Fit_client" onChange={onChangeS2} val={valueS2} min={2} max={10} step={1} />
                                < Slider text="Rounds" onChange={onChangeS3} val={valueS3} min={1} max={10} step={1} />
                                < Slider text="Epochs" onChange={onChangeS4} val={valueS4} min={1} max={20} step={1} />
                            </div>

                            <div className="justify-items-center">
                                <button type="submit" className="btn">Start FL</button>
                            </div>
                            <div className="justify-items-center">
                                {valueS1 <= valueS2 && <Alert />}
                            </div>

                        </div>
                        :
                        < div className="grid gap-4 md:grid-cols-2" >
                            < Dropdown fact={props.factory} handleChange={handleChange} />
                            < Slider text="Min_client" onChange={onChangeS1} val={valueS1} min={2} max={10} step={1} />
                            < Slider text="Fit_client" onChange={onChangeS2} val={valueS2} min={2} max={10} step={1} />
                            < Slider text="Rounds" onChange={onChangeS3} val={valueS3} min={1} max={10} step={1} />
                            < Slider text="Epochs" onChange={onChangeS4} val={valueS4} min={1} max={20} step={1} />
                            <button type="submit" className="btn">Start FL</button>
                        </div>
                    }
                </fieldset>
            </form >
        </div >
    )
}

export default Form;