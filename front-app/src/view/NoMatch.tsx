import { Link } from "react-router-dom";

const NoMatch = (): JSX.Element => {
    return (
        <div className="container mx-auto py-9">
            <div className="flex flex-col space-y-9 text-center">
                <h2 className="mb-8 font-extrabold text-9xl text-orange-600">
                    <span className="sr-only ">Error</span>404
                </h2>
                <p className="text-2xl font-semibold md:text-3xl">Sorry, we couldn't find this page.</p>
                <Link to="" className="flex justify-center">
                    <button type="button" className="btn">
                        Go home
                    </button>
                </Link>
            </div>
        </div>

    );
}

export default NoMatch;