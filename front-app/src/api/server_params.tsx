import axios from "axios";

export const server_params = async (url: string, value: {}): Promise<void> => {
    await axios.post(
        url,
        value,
    )
        .then(function (response) {
            console.log(response);
        })
        .catch(function (error) {
            console.log(error);
        });

}
