import Factories from "../interfaces/factories";
import axios from "axios";


export const getFactories = (url: string) => {
    return new Promise<{ data: Factories }>((resolve, reject) => {
        axios
            .get(url)
            .then((response) => {
                resolve(response.data);
            })
            .catch((error) => {
                console.log("error", error);
                reject(error);
            });
    });

}

export const selectedFactory = async (url: string, value: {}): Promise<void> => {
    await axios.post(
        url,
        value,
    )
        .then(function (response) {
            console.log(response);
        })
        .catch(function (error) {
            console.log(error);
        });

}
