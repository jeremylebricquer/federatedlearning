import axios from "axios";

export const inference = async (url: string, value: FormData, headers: {}) => {
    await axios.post(
        url,
        value,
        headers
    )
        .then(function (response) {
            console.log(response);
        })
        .catch(function (error) {
            console.log(error);
        });

}

