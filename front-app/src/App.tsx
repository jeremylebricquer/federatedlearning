import './App.css'
import React from 'react';
import { Routes, Route } from "react-router-dom";
import { NavBar } from './components/navbar';
import Home from './view/Home'
const FederateLearning = React.lazy(() => import("./view/FederateLearning"))
const NoMatch = React.lazy(() => import("./view/NoMatch"))


const App = (): JSX.Element => {

  return (
    <>
      <NavBar />
      <Routes>
        <Route index element={<Home />} />
        <Route path="" element={<Home />} />
        <Route
          path="/FL"
          element={
            <React.Suspense fallback={<>...</>}>
              <FederateLearning />
            </React.Suspense>
          }
        />
        <Route path="*" element={<NoMatch />} />
      </Routes>
    </>
  )
}



export default App