#!/bin/bash

for i in `seq 0 1`; do
    echo "Del client $i"
    docker rm client_${i}
    #docker kill client_${i} && docker rm client_${i}
done

trap "trap - SIGTERM && kill -- -$$" SIGINT SIGTERM
wait