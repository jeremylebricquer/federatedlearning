#!/bin/bash

server_params=$(curl -X 'GET' 'http://192.168.1.35:8000/server/params' -H 'accept: application/json' | \
python3 -c "import sys, json; params= [];[params.append(value) for value in json.load(sys.stdin)['data'].values()];print(' '.join(params))")

arr=(${server_params})
minClient=${arr[1]}
epochs=${arr[4]}
id=${arr[5]}

function request_port() {
    req=$(curl -X 'GET' "http://192.168.1.35:8000/factories/$1" -H 'accept: application/json'| \
    python3 -c "import sys, json; factories= [];[factories.append(i[-1]) for i in json.load(sys.stdin)['data']] ;print(' '.join(factories))")
    echo $req
}


function run_client(){
  for i in `seq 1 $minClient`; do
      echo "Starting client $i"
      docker run -d -it --name client_${i}_$PORTS \
          -e FLOWER_SERVER_IP="192.168.1.35"\
          -e FLOWER_SERVER_PORT=$PORTS \
          -e MLFLOW_PORT="5000" \
          -e EPOCHS=$epochs \
          --privileged \
          --restart on-failure \
          -v $(pwd)/Client/data/:/app/data/ \
          -v connection:/app/.cache \
          fl_client
  done
}


PORTS=$(request_port $id)

echo "Ports : "$PORTS
run_client $PORT

trap "trap - SIGTERM && kill -- -$$" SIGINT SIGTERM
wait
