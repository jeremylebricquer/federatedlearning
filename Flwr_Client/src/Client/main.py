from pathlib import Path
from time import sleep

from client import start_Client
from utils.var import *


def run_client():

    while True:
        sleep(5)
        start_Client(
            epochs=EPOCHS
        )
        os._exit(0)


if __name__ == "__main__":
    run_client()
