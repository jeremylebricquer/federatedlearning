from socket import gethostname
import os
import torch

DEVICE: str = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
BATCH_SIZE = 32

FLOWER_SERVER_IP = os.environ["FLOWER_SERVER_IP"]
FLOWER_SERVER_PORT = os.environ["FLOWER_SERVER_PORT"]
MLFLOW_PORT = os.environ["MLFLOW_PORT"]
EPOCHS = int(os.environ["EPOCHS"])
MLFLOW_TRACKING_URI = "http://" + FLOWER_SERVER_IP + ":" + MLFLOW_PORT


