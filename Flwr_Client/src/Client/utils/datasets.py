import torchvision.transforms as transforms
from torchvision.datasets import ImageFolder
from torch.utils.data import DataLoader


def load_dataset(
    train_dir: str, val_dir: str = None, test_dir: str = None
) -> ImageFolder:
    """
    Load dataset PyTorch method

    Args:
        train_dir (str): Train directory path
        val_dir (str, optional): Validation directory path. Defaults to None.
        test_dir (str, optional): Test directory path. Defaults to None.

    Returns:
        ImageFolder: Training set , Validation set , Test set
    """
    transform = transforms.Compose(
        [
            transforms.Resize(256),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
        ]
    )

    trainset = ImageFolder(root=train_dir, transform=transform)
    validset = ImageFolder(root=val_dir, transform=transform)
    testset = ImageFolder(root=test_dir, transform=transform)

    return trainset, validset, testset


def datasetloader(trainset, validset, testset, bs: int = 32) -> DataLoader:
    """Transform dataset with Pytorch Dataloader

    Args:
        - trainset (ImageFolder): Loading training set with torchvision.datasets.ImageFolder
        - validset (ImageFolder): Loading validation set with torchvision.datasets.ImageFolder
        - testset (ImageFolder):  Loading test set with torchvision.datasets.ImageFolder
        - BATCH_SIZE (int): batch number

    Returns:
        train_loader , test_loader , valid_loader , num_examples
    """

    train_loader = DataLoader(trainset, batch_size=bs, shuffle=True)
    valid_loader = DataLoader(validset, batch_size=bs, shuffle=False)
    test_loader = DataLoader(testset, batch_size=bs, shuffle=False)
    num_examples = {"trainset": len(trainset), "testset": len(testset)}

    return train_loader, test_loader, valid_loader, num_examples
