# **<center> Federate Learning (Flower Client)</center>**

## Client Side on flower Framework

` `  
` `

## **Structure**

* **Base** : 
    1. **`dataset.py`** : Load img, build pytorch dataloader
    2.  **`model.py`**: Neural network class with pl method   

* **Data** :
    1. **`train`** : Train folder | 733 : images : jpg | Labels : _( good / not good )_
    2. **`val`** : Validation folder | 239 : images : jpg | Labels : _( good / not good )_
    3. **`test`**: Test folder | 52 : images : jpg  | Labels : _( good / not good )_ 
    
* **Entrypoint** :  
    1. **`client.py`** : Flower client class ,  set tracking for server , autolog = ON    
` `  
` `
## **Virtual environnment / Deps**
 
1. **Install virtual env / Create/ Activate:**

```bash
$ pip install virtualenv
$ virtualenv -p python3.8.8 flower_env
$ source flower_env/bin/activate
```
2. **Install dependencies:**

```
$ pip install -r requirements.txt
```   
` `  
` `
## **Training model** 
Run python script 
```
$ py client.py --epochs 3
```
_Note : Default Epochs args = 6_
