import os
import warnings
from collections import OrderedDict
from typing import Dict, List, Tuple

import flwr as fl
import mlflow
import numpy as np
import pytorch_lightning as pl
import torch
import torch.nn as nn
from mlflow import set_tracking_uri
from pytorch_lightning.utilities.warnings import PossibleUserWarning
from torch.optim import Adam
from torch.utils.data import DataLoader
from utils.datasets import datasetloader, load_dataset
from utils.model import Autolog_info, NeuralNetwork, test
from utils.var import *

warnings.filterwarnings("ignore", category=PossibleUserWarning)

PATH = os.getcwd()

set_tracking_uri(MLFLOW_TRACKING_URI)


class Client(fl.client.NumPyClient):
    def __init__(
        self,
        model: NeuralNetwork(),
        trainloader: DataLoader,
        validloader: DataLoader,
        testloader: DataLoader,
        num_examples: Dict,
        criterion: torch.nn,
        optimizer: torch.optim,
        epoch: int,
    ) -> None:
        self.model = model
        self.trainloader = trainloader
        self.validloader = validloader
        self.testloader = testloader
        self.num_examples = num_examples
        self.criterion = criterion
        self.optimizer = optimizer
        self.epoch = epoch

    def get_parameters(self, config) -> List[np.ndarray]:
        self.model.train()
        # Return model parameters as a list of NumPy ndarrays
        return [val.cpu().numpy() for _, val in self.model.state_dict().items()]

    def set_parameters(self, parameters: List[np.ndarray]) -> None:
        # Set model parameters from a list of NumPy ndarrays
        params_dict = zip(self.model.state_dict().keys(), parameters)
        state_dict = OrderedDict({k: torch.tensor(v) for k, v in params_dict})
        self.model.load_state_dict(state_dict, strict=True)

    def fit(
        self, parameters: List[np.ndarray], config: Dict[str, str]
    ) -> Tuple[List[np.ndarray], int, Dict]:
        # Set model parameters, train model, return updated model parameters
        self.set_parameters(parameters)
        # Initialize a trainer
        trainer = pl.Trainer(log_every_n_steps=20, max_epochs=self.epoch)
        # Auto log all MLflow entities
        mlflow.pytorch.autolog()
        # Train the model
        with mlflow.start_run() as run:
            trainer.fit(self.model, self.trainloader, self.validloader)
            Autolog_info(mlflow.get_run(run_id=run.info.run_id))

        return self.get_parameters(config), self.num_examples["trainset"], {}

    def evaluate(
        self, parameters: List[np.ndarray], config: Dict[str, str]
    ) -> Tuple[float, int, Dict]:
        # Set model parameters, evaluate model on local test dataset, return result
        self.set_parameters(parameters)
        loss, accuracy = test(self.model, self.testloader, device=DEVICE, criterion=self.criterion)
        return float(loss), self.num_examples["testset"], {"accuracy": float(accuracy)}


def start_Client(epochs: int = 1) -> None:
    """Flower client initialization

    Args:
        - certificates
        - epochs (int, optional): Defaults to 1.

    Returns:
            None
    """
    model = NeuralNetwork().to(DEVICE)
    criterion = nn.CrossEntropyLoss()
    optimizer = Adam(model.parameters(), lr=1e-3)
    trainset, testset, validset = load_dataset(
        train_dir=PATH + "/data/train",
        val_dir=PATH + "/data/val",
        test_dir=PATH + "/data/test",
    )
    trainloader, validloader, testloader, num_example = datasetloader(
        trainset, validset, testset, BATCH_SIZE
    )

    # Start client
    client = Client(
        model,
        trainloader,
        validloader,
        testloader,
        num_example,
        criterion,
        optimizer,
        epochs,
    )

    server_address = FLOWER_SERVER_IP + ":" + FLOWER_SERVER_PORT
    fl.client.start_numpy_client(
        server_address=server_address,
        client=client,
    )
