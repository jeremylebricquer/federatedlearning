# 1. Local  
## **Test ok**
```
python server.py --r=1 --ac=1 --fc=1
```
```
py client.py --epochs=1
```
```
mlflow server 
    --backend-store-uri postgresql://orange:orange@localhost/mlflow_db 
    --default-artifact-root file:/home/orange/workspace/Flower_FL/mlruns 
    -h 0.0.0.0
```

# 2. Client  docker

## **Test ok**    

server_address="0.0.0.0:8080"
172.17.0.1
```
python server.py --r=1 --ac=1 --fc=1
```
```
docker run -d -it --name client -e FLOWER_SERVER="172.17.0.1" -v $(pwd)/data/:/app/data/ fl_client
```

```
mlflow server 
    --backend-store-uri postgresql://orange:orange@localhost/mlflow_db 
    --default-artifact-root file:/home/orange/workspace/Flower_FL/mlruns 
    -h 0.0.0.0
```

# 3. Server  docker
## **Test ok**    
server_address="0.0.0.0:8080"
172.17.0.1
```
docker run -d -it --name server -p 8080:8080 --restart on-failure -v $(pwd)/artifacts/:/app/artifacts/ fl_server
```
```
docker run -d -it --name client -e FLOWER_SERVER="172.17.0.1" -v $(pwd)/data/:/app/data/ fl_client
```
```
mlflow server 
    --backend-store-uri postgresql://orange:orange@localhost/mlflow_db 
    --default-artifact-root sftp://orange:orange@192.168.1.50$PWD/artifacts 
    -h 0.0.0.0
```
# 4. Postgres
## **Test ok**    
server_address="0.0.0.0:8080"
172.17.0.1
```
docker run -d -it --name server -p 8080:8080 --restart on-failure -v $(pwd)/artifacts/:/app/artifacts/ fl_server
```
```
docker run -d -it --name client -e FLOWER_SERVER="172.17.0.1" -v $(pwd)/data/:/app/data/ fl_client
```
```
mlflow server \
--backend-store-uri postgresql://orange:orange@localhost/mlflow_db \
--default-artifact-root sftp://orange:orange@192.168.1.50$PWD/Server/artifacts \
-h 0.0.0.0
```

```
docker volume create pgdata
```

```
docker run -p 5432:5432 -d \
        --name postgresql_db \
        -e POSTGRES_PASSWORD=orange \
        -e POSTGRES_USER=orange \
        -e POSTGRES_DB=mlflow_db \
        -v pgdata:/var/lib/postgresql/data \
        postgres
```

# 4. Mlflow

## **ok** 

ENV BACKEND_URI postgresql://orange:orange@172.17.0.1:5432/mlflow_db 
ENV ARTIFACT_ROOT sftp://orange:orange@192.168.1.50/home/orange/workspace/Flower_FL/Server/artifacts

SERVER FLOWER
```
docker run -d -it --name server -p 8080:8080 --restart on-failure -v $(pwd)/artifacts/:/app/artifacts/ fl_server
```
SERVER MLFLOW
```
docker run -d -it --name server \
        -p 8080:8080 \
        -p 6000:6000 \
        -e FLOWER_PORT="8080" \
        -e FLASK_PORT="6000"\
        -e ROUNDS=1 \
        -e FIT_CLIENT=2 \
        -e MIN_CLIENT=2 \
        -v $(pwd)/Server/artifacts/:/app/artifacts/ \
        --restart always \
        fl_server
```
CREATION VOLUME BDD
```
docker volume create pgdata
```
DOCKER BDD
```
docker run -p 5432:5432 -d \
        --name postgresql_db \
        -e POSTGRES_PASSWORD=orange \
        -e POSTGRES_USER=orange \
        -e POSTGRES_DB=mlflow_db \
        -v pgdata:/var/lib/postgresql/data \
        postgres
```
Client 
```
docker run -d -it --name client_1 \
        -e FLOWER_SERVER_IP="172.17.0.1"\
        -e FLOWER_SERVER_PORT="8080" \
        -e MLFLOW_PORT="5000" \
        -e FLASK_IP="172.17.0.1" 
        -e FLASK_PORT="6000" \
        -e EPOCHS="2" \
        -v $(pwd)/Client/data/:/app/data/ \
        --privileged \
        fl_client 
```

----

Error 
----
Pb de droit ? 

* privileged sur le client + ssh mlflw server  => ok 
* privileged sur le client ok pour artefact en local mais ui plante 


RUN useradd -m user
RUN mkdir -p /home/user/.ssh
RUN chown -R user:user /home/user/.ssh
RUN echo "Host orange\n\tStrictHostKeyChecking no\n" >> /home/user/.ssh/config
USER user

Pour ajouter lhost 
ssh-keyscan -H 192.168.1.50 >> etc/ssh/known_hosts

/etc/ssh/sshd_config
PasswordAuthentication yes  -> no

Verifier le group et owner du folder artifacts que ce soit le meme que le user
ici orange

Création des 2 clés
/etc/ssh//id_rsa  /etc/ssh//id_rsa.pub
ssh-keygen -t rsa -b 4096 -C "orange@192.168.150"

#Créer un volume piur stocker les clés ssh 
docker run -it --rm -v /etc/ssh/:/etc/ssh/ ssh-test ssh orange@192.168.1.50

docker run -v /tmp/:/keys keygen-container

------- 
Correctif pour le client ?

SFTP Server
To store artifacts in an SFTP server, specify a URI of the form sftp://user@host/path/to/directory. You should configure the client to be able to log in to the SFTP server without a password over SSH (e.g. public key, identity file in ssh_config, etc.).


En priviliged mlflow server pas derreur sur ui pour artifact
En privileged client création ok des artifacts

#! Bon sftp 
sftp://orange:orange@192.168.1.50/home/orange/workspace/Flower_FL/Server/artifacts

Créer un volume avec les clés et le lire avec le 2nd container

