# **<center> Docker</center>**


## **Ordre de lancement**

1.  Serveur Flower
2.  Volume de la bdd / Container database 
3.  Mlflow serveur
4.  Clients Flower

---



## 1. Serveur
Construction de l'image serveur
```
$ docker build -f Server/Dockerfile.server -t fl_server .
```
Lancement du container

```
$ docker run -d -it --name server \
        -p 8080:8080 \
        -e FLOWER_PORT="8080" \
        -e ROUNDS=1 \
        -e FIT_CLIENT=2 \
        -e MIN_CLIENT=2 \
        -v $(pwd)/Server/artifacts/:/app/artifacts/ \
        --restart always \
        fl_server
```
__Note: Flower port 3000x__
Run docker container with bash script 

## 2. Postgresql

Lancement du container postgresql

Création volume 
```
$ docker volume create pgdata
```
Création de la base de la bdd
```
$ docker build -f Database/Dockerfile.database -t database .
```

```
$ docker run -p 5432:5432 -d \
        --name postgresql_db \
        -e POSTGRES_PASSWORD=orange \
        -e POSTGRES_USER=orange \
        -e POSTGRES_DB=mlflow_db \
        -v pgdata:/var/lib/postgresql/data \
        --restart always \
        database
```

## 3. Mlfow

Création de l'image Mlflow 

```
$ docker build -f Server/Dockerfile.mlflow -t mlflow .
```

Lancement du server de tracking 
```
$ docker run -d -it --name mlflow_server -p 5000:5000 --privileged --restart always mlflow  
```

## 4. API

Création de l'image avec fastAPI

```
$ docker build -f API/Dockerfile.api -t api .
```
Création des tables `Customers` et `trials`.  
Lancement du server fastAPI 
```
$ docker run -d -it --name api_server\
        -p 8000:8000 \
        -e DB_NAME='mlflow_db' \
        -e DB_USER='orange' \
        -e DB_PASSWORD='orange' \
        -e DB_HOST="172.17.0.1" \
        -e DB_PORT="5432" \
        --restart always \
        api  
```

## 5. Client

Construction du image servant de base avec toute les dépendances néccessaires
```
$ docker build -f Client/Dockerfile.base -t base_client .
```
Build de l'image client
```
$ docker build -f Client/Dockerfile.client -t fl_client .
```
Lancement du container et ajout des datas sur le volume
```
$ docker run -d -it --name client \
        -e FLOWER_SERVER_IP="172.17.0.1"\
        -e FLOWER_SERVER_PORT="8080" \
        -e MLFLOW_PORT="5000" \
        -e EPOCHS="2" \
        -e EXPERIMENT_NAME="exp_1"\
        -v $(pwd)/Client/data/:/app/data/ \
        --privileged \
        fl_client 
```


## 6. Inference

Buil de l'image inference
```
$ docker build -f Inference/Dockerfile.inference -t inf_img .
```
Lancement du container et ajout des datas sur le volume
```
$ docker run -d -it --name inference inf_img
```


---



## . Volume connexion ssl et ssh

```
$ docker volume create connection
```

```
$ docker build -f Dockerfile.ssl -t ssl .
```

```
$ docker run -d -it --name c_ssl -v connection:/app/.cache ssl
```
