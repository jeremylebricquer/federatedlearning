# **<center> Federated Learning  </center>**

  
## Description

Federated Learning for computer vision using Flower and Pytorch. 
This repository implements several models and custom strategies for federated learning in computer vision using flower for multilabel classification.

## How to install
```
git clone https://gitlab.com/jeremylebricquer/federatedlearning.git
```
## Getting Started


* [Flower](https://flower.dev/docs/) : Federate learning framework   
* [Mlflow](https://mlflow.org/docs/latest/index.html) : Logging model / metrics   
* [Pytorch](https://pytorch.org/docs/stable/index.html) : Deep learning framework  
* [Docker](https://docs.docker.com/language/python/) : Container  


## 2- Setup with docker 

See [docker Readme](./docs/readme_docker.md)

## 3- How to run  Server
See [Server Readme](./Flwr_Server/src/Server/readme.md)

## 4- How to run client 
See [Client Readme](./Flwr_Client/src/Client/readme.md)


### File structure

```text
Flower_FL/
└──
    ├──docs/
    │  └── readme_docker.md
    │ 
    Flwr_Server/
    ├── src/
    │   ├── API/
    │   │   ├── database.py 
    │   │   ├── utils.py 
    │   │   ├── Dockerfile.api
    │   │   └── main.py
    │   ├── Database/
    │   │   ├── init_db.sh
    │   │   ├── Dockerfile.database
    │   │   └── main.py
    │   ├── Inference/
    │   │   ├── requirements.txt 
    │   │   ├── Dockerfile.inference
    │   │   └── inference.py
    │   └── Server/
    │       ├── artifacts
    │       ├── utils
    │       │    ├── datasets.py
    │       │    ├── model.py
    │       │    └── utils.py
    │       ├── Dockerfile.mlflow
    │       ├── Dockerfile.server
    │       ├── run_server.sh.
    │       ├── server.py 
    │       ├── server.py 
    │       ├── Dockerfile.mlflow
    │       ├── Dockerfile.server
    │       ├── requirements_server.txt
    │       ├── readme.md
    │       └── main.py
    │ 
    Flwr_Client/
    ├── src/
    │   ├── certificates/
    │   │   ├── certificate.conf
    │   │   └── generate.sh
    │   ├── Client/
    │   │   ├── data
    │   │   │   ├── train
    │   │   │   ├── validation
    │   │   │   └── test
    │   │   ├── utils
    │   │   │   ├── datasets.py
    │   │   │   ├── model.py
    │   │   │   └── var.py
    │   │   ├── client.py
    │   │   ├── Dockerfile.base
    │   │   ├── Dockerfile.client
    │   │   ├── requirements.txt
    │   │   ├── readme.md
    │   │   └── main.py
    │   ├── del_client.sh
    │   └── run.sh
    │
    Front/
    ├── src/
    │   ├── api/
    │   ├── assets
    │   ├── components/
    │   ├── interfaces/
    │   ├── view/
    │   ├── App.css
    │   ├── App.tsx
    │   ├── Index.css
    │   ├── main.tsx
    └── index.html
    readme.md

```

## Help

If you want to run server without docker make sure you have the necessary requirements for *Python* packages and at least *Python3.8*

```
python -m pip install requirements.txt
```