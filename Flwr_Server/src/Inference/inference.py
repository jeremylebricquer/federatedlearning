import torch
import cv2
import torchvision.transforms as transforms
import requests

headers = {
    "content-type": "application/json",
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko)",
}

# request = requests.get("http://localhost:8000/inference/", headers)
# print("request", request)

print("ok")

""" PATH = ""

# the computation device
device = "cuda" if torch.cuda.is_available() else "cpu"
# list containing all the class labels
labels = ["good", "not_good"]

# initialize the model and load the trained weights
model = torch.load(PATH)
model.eval()

# define preprocess transforms
transform = transforms.Compose(
    [
        transforms.ToPILImage(),
        transforms.Resize(224),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.5, 0.5, 0.5], std=[0.5, 0.5, 0.5]),
    ]
)

img_path = "data/test/not_good/2.jpg"
# read and preprocess the image
image = cv2.imread(img_path)
# get the ground truth class
classes = img_path.split("/")[-2]
orig_image = image.copy()
# convert to RGB format
image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
image = transform(image)
# add batch dimension
image = torch.unsqueeze(image, 0)
with torch.no_grad():
    outputs = model(image.to(device))
output_label = torch.topk(outputs, 1)
pred_class = labels[int(output_label.indices)]
cv2.putText(
    orig_image,
    f"Label: {classes}",
    (10, 25),
    cv2.FONT_HERSHEY_SIMPLEX,
    0.6,
    (0, 255, 0),
    2,
    cv2.LINE_AA,
)
cv2.putText(
    orig_image,
    f"Pred: {pred_class}",
    (10, 55),
    cv2.FONT_HERSHEY_SIMPLEX,
    0.6,
    (0, 0, 255),
    2,
    cv2.LINE_AA,
)
print(f"Label: {classes}, pred: {pred_class}")
cv2.imshow("Result", orig_image)
cv2.waitKey(0)
cv2.imwrite(f"outputs/{classes}{img_path.split('/')[-1].split('.')[0]}.png", orig_image) """
