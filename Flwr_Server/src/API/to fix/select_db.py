import psycopg2
from psycopg2.extensions import connection
from .conf import *
from typing import Union


def table_exists(conn: connection, table: str) -> bool:
    """Check if given table exist

    Args:
        - conn (psycopg2.extensions.connection): db connection
        - table (str): table name

    Returns:
        bool: return in table exist or not
    """
    exists = False
    try:
        cur = conn.cursor()
        cur.execute(
            "select exists(select relname from pg_class where relname='" + table + "')"
        )
        exists = cur.fetchone()[0]
        cur.close()
    except psycopg2.Error as e:
        print(e)
    return exists


def track_exists(conn: connection, name: str) -> bool:
    exists = False
    try:
        cur = conn.cursor()
        cur.execute(
            "select exists(SELECT name FROM customers where name ='" + name + "');"
        )
        exists = cur.fetchone()[0]
        cur.close()
    except psycopg2.Error as e:
        print(e)
    return exists


def select_access_by_id(conn: connection, id: int) -> list:
    """Select access key by customer id

    Args:
        - conn (psycopg2.extensions.connection): db connection
        - id ( int ) : active customer id
    Returns:
        list: return customer access
    """
    try:
        cur = conn.cursor()
        cur.execute(
            "SELECT cacrt , serverpem , serverkey FROM customers WHERE id= "
            + str(id)
            + ";"
        )
        access = cur.fetchall()
        cur.close()
        return access
    except psycopg2.Error as e:
        print(e)


def select_existed_id(conn: connection, table: str, customer_name: str) -> int:
    """Check if given customer name exist and return his id

    Args:
        - conn (psycopg2.extensions.connection): db connection
        - customer_name (str): customer name

    Returns:
        int: return customer id if exist
    """
    try:
        cur = conn.cursor()
        cur.execute("SELECT id FROM " + table + " WHERE name='" + customer_name + "';")
        ids = cur.fetchone()
        cur.close()
        return ids
    except psycopg2.Error as e:
        print(e)


def select_trials_id(conn: connection, customer_id: int) -> list:
    """Check if given customer id have existing experiments id in trials table

    Args:
        - conn (psycopg2.extensions.connection): db connection
        - customer_id (str): customer id

    Returns:
        int: return customer id if exist
    """
    cur = conn.cursor()
    try:
        cur.execute(
            "SELECT experiment_id FROM trials WHERE customer_id="
            + str(customer_id)
            + ";"
        )
        customer_ids = cur.fetchall()
        cur.close()
        return customer_ids
    except psycopg2.Error as e:
        print(e)


def select_item(conn: connection, column: Union[str, list], table: str) -> list:
    """Select all id in given table

    Args:
        - conn (psycopg2.extensions.connection): db connection
    Returns:
        list: return customer id
    """

    if type(column) is str:
        col = column
    elif type(column) is list:
        col = ", ".join(column)

    try:
        cur = conn.cursor()
        sql = "SELECT " + col + " FROM " + table + " ;"
        cur.execute(sql)
        ids = cur.fetchall()
        cur.close()
        return ids

    except psycopg2.Error as e:
        print(e)


def select_ts(conn: connection, id: int):
    """Select all id in given table

    Args:
        - conn (psycopg2.extensions.connection): db connection
    Returns:
        list: return customer id
    """
    try:
        cur = conn.cursor()
        sql = "SELECT time FROM customers where id =" + str(id) + ";"
        cur.execute(sql)
        ts = cur.fetchone()
        cur.close()
        return ts

    except psycopg2.Error as e:
        print(e)
