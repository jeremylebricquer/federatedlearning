import psycopg
from .conf import *


def create_db() -> None:
    """Create new database

    Return
        None
    """
    conn = psycopg.connect(
        database="postgres",
        user=DB_USER,
        password=DB_PASSWORD,
        host=DB_HOST,
        port=DB_PORT,
    )
    cur = conn.cursor()

    conn.autocommit = True
    sql_query = f"CREATE DATABASE {DB_NAME}"

    try:
        cur.execute(sql_query)
        cur.close()
    except Exception as e:
        # print(f"{type(e).__name__}: {e}")
        cur.close()
    else:
        # Revert autocommit settings
        conn.autocommit = False
    finally:
        conn.close()


def create_table(
    conn: psycopg.Connection,
    sql_query: str,
) -> None:
    """Create new table

    Args:
        - conn (psycopg.Connection): connection to db
        - sql_query (str): query
    Returns:
            None
    """
    cur = conn.cursor()
    try:
        cur.execute(sql_query)
        cur.close()
    except Exception as e:
        # print(f"\n {type(e).__name__}: {e}")
        conn.rollback()
    else:
        conn.commit()





def insert_timestamps(conn: psycopg.Connection, time: float, id: int) -> None:
    """Insert customer timestamp when training end

    Args:
        - conn (psycopg.Connection): db connection
        - time(dt.timestamp()): timestamp current fl customer
        - id (int) : current customer id FL
    """
    cur = conn.cursor()
    try:
        sql = "UPDATE customers SET time = " + str(time) + " where id= " + str(id) + ";"
        cur.execute(sql)
        cur.close()

    except Exception:
        conn.rollback()

    finally:
        conn.commit()
