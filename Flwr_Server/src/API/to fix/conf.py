import os

DB_NAME = os.environ["DB_NAME"]
DB_USER = os.environ["DB_USER"]
DB_PASSWORD = os.environ["DB_PASSWORD"]
DB_HOST = os.environ["DB_HOST"]
DB_PORT = os.environ["DB_PORT"]
FLASK_IP = os.environ["FLASK_IP"]
FLASK_PORT = os.environ["FLASK_PORT"]

url = "http://" + FLASK_IP + ":" + FLASK_PORT

headers = {
    "Content-Type": "application/json",
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko)",
}

certif_dir = ".cache/certificates/"

factories = """
    CREATE TABLE customers
    (
        id SERIAL PRIMARY KEY,
        name VARCHAR(200) UNIQUE NOT NULL,
        cacrt VARCHAR(200),
        serverpem VARCHAR(200),
        serverkey VARCHAR(200),
        time float
    )"""


trials = """
    CREATE TABLE trials
    (
        customer_id INT, 
        experiment_id INT,
        CONSTRAINT fk_customer 
            FOREIGN KEY(customer_id)
                REFERENCES customers (id),
        CONSTRAINT fk_experiment 
            FOREIGN KEY(experiment_id)
                REFERENCES experiments (experiment_id)

    )
    """
