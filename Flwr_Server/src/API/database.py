import psycopg
import sys
from utils import DB_HOST, DB_NAME, DB_PASSWORD, DB_PORT, DB_USER
from typing import Union
from psycopg.sql import SQL, Identifier


class Database:
    def __init__(self):
        self.conn = self.connect()

    def connect(self):
        """
        Connect to database and return connection
        """
        print("Connecting to PostgreSQL Database...")
        try:
            conn = psycopg.connect(
                host=DB_HOST,
                dbname=DB_NAME,
                user=DB_USER,
                password=DB_PASSWORD,
                port=DB_PORT,
            )
        except psycopg.OperationalError as e:
            print(f"Could not connect to Database: {e}")
            sys.exit(1)

        return conn

    def table_exists(self, table: str) -> bool:
        """Check if given table exist

        Args:
            - conn (psycopg2.extensions.connection): db connection
            - table (str): table name

        Returns:
            bool: return in table exist or not
        """
        exists = False
        try:
            cur = self.conn.cursor()
            SQL = "select exists(select relname from pg_class where relname= %s)"
            cur.execute(SQL, (table,))
            exists = cur.fetchone()[0]
            cur.close()
        except psycopg.Error as e:
            print(e)
        return exists


def select_all_items(conn: psycopg.Connection, table: str) -> list:
    """Select all items in given table

    Args:
        - conn (psycopg.Connection): db connection
    Returns:
        list: return item list
    """

    try:
        cur = conn.cursor()
        query = SQL("SELECT * FROM {};").format(Identifier(table))
        cur.execute(query)
        ids = cur.fetchall()
        cur.close()
        return ids

    except psycopg.Error as e:
        print(e)


def select_item(conn: psycopg.Connection, table: str, id: int) -> list:
    """Select specific item in given table

    Args:
        - conn (psycopg.Connection): db connection
    Returns:
        list: return item
    """

    try:
        cur = conn.cursor()
        query = SQL("SELECT * FROM {} WHERE id={};").format(Identifier(table), id)
        cur.execute(query)
        id = cur.fetchall()
        cur.close()
        return id

    except psycopg.Error as e:
        print(e)


def selected(conn: psycopg.Connection, column: Union[str, list], table: str) -> list:
    """Select info in given table

    Args:
        - conn (psycopg.Connection): db connection
    Returns:
        list: return item list
    """
    if type(column) is str:
        col = column
    elif type(column) is list:
        col = ", ".join(column)

    try:
        cur = conn.cursor()
        query = SQL("SELECT {} FROM {};").format(Identifier(col), Identifier(table))
        cur.execute(query)
        ids = cur.fetchall()
        cur.close()
        return ids

    except psycopg.Error as e:
        print(e)


def insert_ports(conn: psycopg.Connection, ports: str, name: str) -> None:
    """Insert port in factory table

    Args:
        - conn (psycopg.Connection): db connection
        - name (str) : current factory name
        - port(str): current docker port

    """
    cur = conn.cursor()
    try:
        sql = "UPDATE factories SET ports = %s where name= %s;"
        cur.execute(sql, (ports, name))
        cur.close()

    except Exception:
        conn.rollback()

    finally:
        conn.commit()


def insert_factories(
    conn: psycopg.Connection, name: str, cacrt: str, serverpem: str, serverkey: str
) -> int:
    """Insert fresh data

    Args:
        - conn (psycopg.Connection): db connection
        - name(str): name
        - cacrt (str): server certificat
        - serverpem (str): server pem
        - serverkey (str): server key
    Return
        (int) id
    """
    cur = conn.cursor()
    try:
        sql = "INSERT INTO factories(name, cacrt, serverpem, serverkey) VALUES(%s,%s,%s,%s) RETURNING id;"
        cur.execute(sql, (name, cacrt, serverpem, serverkey))
        id = cur.fetchone()[0]
        cur.close()
        return int(id)

    except Exception as error:
        # print(f"{type(error).__name__}: {error}")
        conn.rollback()

    finally:
        conn.commit()


def insert_trials(
    conn: psycopg.Connection,
    customer_id: int,
    experiment_id: int,
) -> None:
    """Insert fresh data

    Args:
        - conn (psycopg.Connection): db connection
        - customer_id (int): customer_id
        - experiment_id(int): experiment_id
    """
    cur = conn.cursor()
    try:
        sql = "INSERT INTO trials(customer_id,experiment_id)VALUES(%s,%s);"
        cur.execute(sql, (customer_id, experiment_id))
        cur.close()

    except Exception as error:
        # print(f"{type(error).__name__}: {error}")
        conn.rollback()

    else:
        conn.commit()


def select_item_by_name(conn: psycopg.Connection, table: str, name: str) -> int:
    """Select specific item in given table

    Args:
        - conn (psycopg.Connection): db connection
    Returns:
        list: return id
    """

    try:
        cur = conn.cursor()
        query = SQL("SELECT id FROM {} WHERE name={};").format(Identifier(table), name)
        cur.execute(query)
        id = cur.fetchall()
        cur.close()
        return id

    except psycopg.Error as e:
        print(e)


def create_table(
    conn: psycopg.Connection,
    sql_query: str,
) -> None:
    """Create new table

    Args:
        - conn (psycopg.Connection): connection to db
        - sql_query (str): query
    Returns:
            None
    """
    cur = conn.cursor()
    try:
        cur.execute(sql_query)
        cur.close()
    except Exception as e:
        # print(f"\n {type(e).__name__}: {e}")
        conn.rollback()
    else:
        conn.commit()


def table_exists(conn: psycopg.Connection, table: str) -> bool:
    """Check if given table exist

    Args:
        - conn (psycopg2.extensions.connection): db connection
        - table (str): table name

    Returns:
        bool: return in table exist or not
    """
    exists = False
    try:
        cur = conn.cursor()
        cur.execute("select exists(select relname from pg_class where relname='" + table + "')")
        exists = cur.fetchone()[0]
        cur.close()
    except psycopg.Error as e:
        print(e)
    return exists
