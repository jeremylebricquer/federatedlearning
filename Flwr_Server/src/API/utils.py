from typing import List, Tuple
import os

DB_NAME = os.environ["DB_NAME"]
DB_USER = os.environ["DB_USER"]
DB_PASSWORD = os.environ["DB_PASSWORD"]
DB_HOST = os.environ["DB_HOST"]
DB_PORT = os.environ["DB_PORT"]

certif_dir = "./cache/certificates/"

factories = """
    CREATE TABLE factories
    (
        id SERIAL PRIMARY KEY,
        name VARCHAR(200) UNIQUE NOT NULL,
        cacrt VARCHAR(200),
        serverpem VARCHAR(200),
        serverkey VARCHAR(200),
        time float
    )"""


trials = """
    CREATE TABLE trials
    (
        factories_id INT, 
        experiment_id INT,
        CONSTRAINT fk_factories 
            FOREIGN KEY(factories_id)
                REFERENCES factories (id),
        CONSTRAINT fk_experiment 
            FOREIGN KEY(experiment_id)
                REFERENCES experiments (experiment_id)

    )
    """


def request_to_list(liste: List[Tuple[int, int]]) -> list:
    """Convert List of tuple to list of int

    Args:
        liste (List[Tuple[int,int]]): list with postgresql result (tuple)

    Returns:
        list: int list
    """

    map_list = map(lambda x: x[0], liste)
    return list(map_list)


def listToString(s):
    for i in s:
        return " ".join(str(i))
