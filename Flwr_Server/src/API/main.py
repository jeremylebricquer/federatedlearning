from typing import Any, Dict, List, Union
from fastapi import FastAPI, File
from fastapi.middleware.cors import CORSMiddleware
from utils import factories, trials, listToString, request_to_list, certif_dir
from PIL import Image
import io

from database import (
    Database,
    create_table,
    select_all_items,
    select_item,
    insert_ports,
    select_item_by_name,
    table_exists,
)

global s_params, images
images = None
s_params = None
tableList = [factories, trials]

app = FastAPI()
db = Database()
origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


isExist = table_exists(db.connect(), "experiments")

if isExist == True:
    for i in tableList:
        create_table(db.connect(), i)


## populate tables
""" insert_customer(
    db.connect(),
    "Factory1",
    certif_dir + "ca.crt",
    certif_dir + "server.pem",
    certif_dir + "server.key",
) """

""" insert_trials(
    db.connect(),
)
 """


@app.get("/factories/")
def read_factory():
    factories = select_all_items(db.connect(), "factories")
    return {"data": factories}


@app.post("/factories/port")
def factory_ports(request: Union[List, Dict, Any] = None):
    [[key, value]] = ((str(key), str(value)) for key, value in request.items())
    insert_ports(db.connect(), value, key)
    return {"message": "Ports successfully added"}


@app.get("/factories/{id}")
def read_factory(id):
    data = select_item(db.connect(), "factories", id)
    return {"data": data}


@app.post("/server/params")
def server_params(request: Dict):
    global s_params
    s_params = request
    return {"message": "Server params successfully added"}


@app.get("/server/params")
def server_params():
    if s_params != None:
        id = select_item_by_name(db.connect(), "factories", s_params["name"])
        id = listToString(request_to_list(id))
        s_params.update({"id": id})
        return {"data": s_params}
    else:
        return {"message": "No params available"}


@app.post("/inference/")
def upload(file: bytes = File(...)):
    global images
    images = Image.open(io.BytesIO(file))
    return {"msg": "Upload complete"}


@app.get("/inference/")
def upload():
    # api call to inference script
    return {"msg": images}
