# **<center>Federate Learning (Flower server)</center>**


## **Virtual env**
Create / Activate virtual env
```bash
$ pip3 install virtualenv
$ virtualenv -p python3.8 flower_env
$ source flower_env/bin/activate
```

Install python dependencies
```bash 
$ pip install -r requirements.txt
```

## **Postgresql**


1ère connection et création de la base postgresql

- Création d'un user / role / privilege 
- Création de la db 

1- Soit via le terminal
```bash
$ sudo -u postgres createuser <username>
$ sudo -u postgres createdb <db_name>
$ sudo -u postgres psql
```sudo 

```psql
psql=# CREATE USER <username> WITH ENCRYPTED PASSWORD '<password>' ;
psql=# GRANT ALL PRIVILEGES ON DATABASE <db_name> TO <username> ;
```

2- Soit via PSQL

```psql
psql=# CREATE DATABASE <your_db_name>
psql=# CREATE USER <your_username> WITH ENCRYPTED PASSWORD '<password>' ;
psql=# GRANT ALL PRIVILEGES ON DATABASE <your_db_name> TO <username> ;
```


Connection avec le user orange sur la db mlflow_db
```
$ psql -U orange mlflow_db
```
 
## **MLflow server**

Lancer le server de tracking MLFlow

```
mlflow server \
--backend-store-uri postgresql://orange:orange@localhost/mlflow_db \
--default-artifact-root sftp://orange:orange@192.168.1.50$PWD/artifacts \
-h 0.0.0.0
```

## **Python**

```
$ python server.py --r=1 --ac=1 --fc=1
```

