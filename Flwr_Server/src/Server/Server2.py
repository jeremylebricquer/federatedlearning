import flwr as fl
from utils.datasets import datasetloader, load_dataset
from utils.utils import get_eval_fn, weighted_average
from flwr.server.history import History


def start_Server(
    server_address: str, rounds: int, fc: int, ac: int, certif: list
) -> History:
    """
    This function starts the server and waits for the client to connect

    :param rounds: The number of rounds to play
    :param fc: Min fit clients, min number of clients to be sampled next round
    :param ac: Min available clients to start
    :ca_rt:server certificate
    :server_pem:server pem
    :server_key:server key
    """
    # Load evaluation data
    _, _, test = load_dataset(None, None, "./data/test")
    _, _, testset = datasetloader(None, None, test)

    # Start Flower server for four rounds of federated learning
    strategy = fl.server.strategy.FedAvg(
        fraction_fit=ac / fc,
        min_fit_clients=fc,
        min_available_clients=ac,
        eval_fn=get_eval_fn(testset),
        evaluate_metrics_aggregation_fn=weighted_average,
    )

    fl.server.start_server(
        server_address=server_address,
        config=fl.server.ServerConfig(num_rounds=rounds),
        strategy=strategy,
        certificates=certif,
    )
