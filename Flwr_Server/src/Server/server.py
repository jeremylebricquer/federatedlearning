import flwr as fl
from typing import Tuple, List
from flwr.common import Metrics
from flwr.server.history import History


def weighted_average(metrics: List[Tuple[int, Metrics]]) -> Metrics:
    # Multiply accuracy of each client by number of examples used
    accuracies = [num_examples * m["accuracy"] for num_examples, m in metrics]
    examples = [num_examples for num_examples, _ in metrics]

    # Aggregate and return custom metric (weighted average)
    return {"accuracy": sum(accuracies) / sum(examples)}


def start_Server(server_address: str, rounds: int, fc: int, ac: int) -> History:
    """
    This function starts the server and waits for the client to connect

    :param rounds: The number of rounds to play
    :param fc: Min fit clients, min number of clients to be sampled next round
    :param ac: Min available clients to start
    """

    # Start Flower server for four rounds of federated learning
    strategy = fl.server.strategy.FedAvg(
        fraction_fit=ac / fc,
        min_fit_clients=fc,
        min_available_clients=ac,
        evaluate_metrics_aggregation_fn=weighted_average,
    )

    fl.server.start_server(
        server_address=server_address,
        config=fl.server.ServerConfig(num_rounds=rounds),
        strategy=strategy,
    )
