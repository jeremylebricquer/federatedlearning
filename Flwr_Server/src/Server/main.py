import os
from server import start_Server

FLOWER_PORT = os.environ["FLOWER_PORT"]
MIN_CLIENT = int(os.environ["MIN_CLIENT"])


def fl_server() -> None:
    """
    `fl_server` starts a server that serves the files in the current directory

    :param args: The command line arguments
    """

    rounds = int(os.environ["ROUNDS"])
    fc = int(os.environ["FIT_CLIENT"])
    ac = int(MIN_CLIENT)

    while True:
        start_Server(server_address="0.0.0.0:" + FLOWER_PORT, rounds=rounds, fc=fc, ac=ac)


if __name__ == "__main__":
    fl_server()
