import sys
from time import sleep

import psycopg2
import requests

from utils.db import *
from utils.db_conf import *

if __name__ == "__main__":

    conn = psycopg2.connect(
        database=DB_NAME,
        user=DB_USER,
        password=DB_PASSWORD,
        host=DB_HOST,
        port=DB_PORT,
    )

    cur = conn.cursor()

    isExist = table_exists(conn, "experiments")

    while True:
        if isExist == True:
            create_table(conn, customer_sql)
            create_table(conn, trial_sql)
            print("Create table OK ")
            break

    while True:
        status = requests.get(url=f"{url}/server/status", headers=headers)
        sleep(5)
        if status.text == "True":
            id_customer = select_id(conn, "customers", "Orange")
            if id_customer != None:
                experiments = requests.get(
                    url=f"{url}/client/experiments", headers=headers
                )
                list_exp = select_trials_id(conn, id_customer[0])
                if int(experiments.text) not in list_exp:
                    insert_trials(conn, id_customer, int(experiments.text))
            else:
                id_customer = insert_customer(
                    conn,
                    "Orange",
                    certif_dir + "ca.crt",
                    certif_dir + "server.pem",
                    certif_dir + "server.key",
                )
                experiments = requests.get(
                    url=f"{url}/client/experiments", headers=headers
                )
                insert_trials(conn, id_customer, int(experiments.text))
            print("Insert data OK ")
            break

    conn.close()
    sys.exit(0)
