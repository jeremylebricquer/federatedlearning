#!/bin/bash
ssl_array=".cache/certificates/ca.crt .cache/certificates/server.pem .cache/certificates/server.key"

starting_port=30001
ending_port=30999

customers=$(curl -X 'GET' 'http://127.0.0.1:8000/factories/' -H 'accept: application/json' | \
python3 -c "import sys, json; factories= [];[factories.append(i[1]) for i in json.load(sys.stdin)['data']] ;print(' '.join(factories))")


server_params=$(curl -X 'GET' 'http://127.0.0.1:8000/server/params' -H 'accept: application/json' | \
python3 -c "import sys, json; params= [];[params.append(value) for value in json.load(sys.stdin)['data'].values()];print(' '.join(params))")


arr=(${server_params})

name=${arr[0]}
minClient=${arr[1]}
fitClient=${arr[2]}
rounds=${arr[3]}


function EPHEMERAL_PORT() {
    LOW_BOUND=30001
    RANGE=998
    while true; do
        CANDIDATE=$[$LOW_BOUND + ($RANDOM % $RANGE)]
        (echo "" >/dev/tcp/127.0.0.1/${CANDIDATE}) >/dev/null 2>&1
        if [ $? -ne 0 ]; then
            echo $CANDIDATE
            break
        fi
    done
}

#for i in ${customers[*]};do
#    PORT=$(EPHEMERAL_PORT)
#    docker run -d -it --name server_$i \
#        -p $PORT:8080 \
#        -e FLOWER_PORT="8080" \
#        -e ROUNDS=$rounds \
#        -e FIT_CLIENT=$fitClient \
#        -e MIN_CLIENT=$minClient \
#        -v $(pwd)/Server/artifacts/:/app/artifacts/ \
#        -v connection:/app/.cache\
#        --restart always \
#        fl_server
#    curl -X POST -H "Content-Type: application/json" -d '{"'$i'": "'$PORT'"}' 'http://127.0.0.1:8000/factories/'
#    printf "\n== Started FL server for $i on port $PORT ==\n"
#done

PORT=$(EPHEMERAL_PORT)
docker run -d -it --name server_$name \
    -p $PORT:8080 \
    -e FLOWER_PORT="8080" \
    -e ROUNDS=$rounds \
    -e FIT_CLIENT=$fitClient \
    -e MIN_CLIENT=$minClient \
    -v $(pwd)/Server/artifacts/:/app/artifacts/ \
    -v connection:/app/.cache\
    --restart always \
    fl_server
curl -X POST -H "Content-Type: application/json" -d '{"'$name'": "'$PORT'"}' 'http://127.0.0.1:8000/factories/port'
printf "\n== Started FL server for $name on port $PORT ==\n"


