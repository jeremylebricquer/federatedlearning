#!/bin/bash
server_params=$(curl -X 'GET' 'http://127.0.0.1:8000/server/params' -H 'accept: application/json' | \
python3 -c "import sys, json; params= [];[params.append(value) for value in json.load(sys.stdin)['data'].values()];print(' '.join(params))")


echo "server params" $server_params

for i in ${server_params[*]}; do
  echo $i
done

#python3 -c "import sys, json; print(json.load(sys.stdin)['data'])")